//
//  SearchMovesViewController.swift
//  Shedd Case Study
//
//  Created by Admin on 09/08/2018.
//  Copyright © 2018 Shedd. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
class SearchMovesViewController: UIViewController {
    
    @IBOutlet weak var topBarView: UIView!
    @IBOutlet weak var searchingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var resultTaleView: UITableView!
    @IBOutlet weak var searchResultView: UIView!
    @IBOutlet weak var searchResultImage: UIImageView!
    @IBOutlet weak var searchResultLabel: UILabel!
    
    
    var searching:Bool = false
    var listingHistory:Bool = true
    var keyBoardHeight:CGFloat = 0.0
    var searchPage = 1
    var arrayFilms =  [NSDictionary]()
    var arrayHistory =  [String]()
    var pullRefreshValue = 100
    var loadingView:LoadingView? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //disabling Alamofire Cache, remove this line if you want to store the request result in cache
        URLCache.shared = URLCache(memoryCapacity: 0, diskCapacity: 0, diskPath: nil)
        
        //GET THE HISTORY
        arrayHistory = Manager.getSeachToHistory()
        
        //SETUP SEARCH FIELD
        self.searchTextField.delegate = self;
        self.searchingIndicator.isHidden = true
        
        //SETUP THE TABLE VIEW
        self.resultTaleView.delegate = self;
        self.resultTaleView.dataSource = self;
        self.resultTaleView.rowHeight = UITableViewAutomaticDimension
        self.resultTaleView.estimatedRowHeight = 151
        let footerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: 30));
        self.resultTaleView.tableFooterView = footerView;
        
        //SETUP LOADINGVIEW
        self.loadingView = UINib.init(nibName: "loadingCircles", bundle: nil).instantiate(withOwner: self)[0] as? LoadingView
        self.loadingView?.frame.size = CGSize.init(width: 30, height: 30)
        self.loadingView?.frame.origin = CGPoint.init(x: footerView.frame.size.width/2-15, y: 0);
        footerView.addSubview(loadingView!)
        self.loadingView?.isHidden = true
        
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if self.keyBoardHeight == 0.0 {
            NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
            self.searchTextField.becomeFirstResponder()
        }
    }
    
    //MANAGE TABLE SIZE WHEN KEYBOARD SIZE CHANGE
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            
            if self.keyBoardHeight == 0.0
            {
                let keyboardHeightGet = keyboardSize.height
                self.keyBoardHeight = keyboardHeightGet
                var rect = self.resultTaleView.frame;
                rect.size.height = rect.size.height - self.keyBoardHeight+1;
                resultTaleView.frame = rect
            }
            
        }
    }
  
    func findMoviesWithQuery(fromSearch: Bool, queryString:String){
        searching = true
        self.searchingIndicator.isHidden = false;
        self.searchingIndicator.startAnimating()
        var pageAux:Int = self.searchPage;
        if !fromSearch {
            pageAux += 1
        }
        Manager.findMoviesFromApiRest(queryString: queryString, page: pageAux) { (filmsArray, error) in
            self.searching = false
            self.searchingIndicator.isHidden = true
            if error != nil
            {
                let errorDescription = error?.localizedDescription
                if errorDescription != "cancelled"
                {
                    self.resultTaleView.isHidden = true
                    self.searchResultView.isHidden = false
                    self.searchResultLabel.text = "Upss... something was wrong"
                    self.searchResultImage.image = UIImage.init(named: "search_status_error")
                }
            }
            else{
                if fromSearch
                {
                    self.arrayFilms = filmsArray
                    self.searchPage = 1
                }
                else{
                    if (filmsArray.count>0){
                        self.arrayFilms.append(contentsOf: filmsArray)
                        self.searchPage = pageAux
                    }
                }
                if self.arrayFilms.count>0 {
                    self.resultTaleView.isHidden = false
                    self.searchResultView.isHidden = true
                }
                else {
                    self.resultTaleView.isHidden = true
                    self.searchResultView.isHidden = false
                    self.searchResultLabel.text = "Your search don't have results"
                    self.searchResultImage.image = UIImage.init(named: "search_status_not_found")
                }
                self.resultTaleView.reloadData()
                self.loadingView?.stopAnimate()
                self.loadingView?.Reset()
                self.loadingView?.isHidden = true
            }
        }
        
    }
    
    // MARK: - actionS -------------------
    
    @IBAction func SearchButtonPressed(_ sender: Any) {
        self.searchTextField.resignFirstResponder()
        self.resultTaleView.frame.size.height =   self.resultTaleView.frame.size.height + self.keyBoardHeight-1;
        if searchTextField.text != ""{
            Manager.saveSeachToHistory(queryString: searchTextField.text!.trimmingCharacters(in: CharacterSet.whitespaces))
        }
    }
    
}


// MARK: - ResultTableView delegate -------------------
extension SearchMovesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if listingHistory {
            return arrayHistory.count
        }
        return arrayFilms.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if listingHistory {//IF SHOULD LIST HISTORY
            var cell = tableView.dequeueReusableCell(withIdentifier: "CellHistory")
            if(cell == nil)
            {
                cell =  UITableViewCell.init(style: UITableViewCellStyle.default, reuseIdentifier: "CellHistory")
            }
            cell?.textLabel?.text = arrayHistory[indexPath.row]
            return cell!
        }
        else//IF SHOULD LIST FILMS
        {
            let dictFilm = arrayFilms[indexPath.row]
            if let cell  = tableView.dequeueReusableCell(withIdentifier: "CellFilmID", for: indexPath) as? CellFilm{
                cell.item = dictFilm
                return cell
            }
        }
        return UITableViewCell()
    }
}



// MARK: - ResultTableView delegate -------------------
extension SearchMovesViewController: UITableViewDelegate {
    
    //MANAGING THE SCROLL TO SHOW LOADING VIEW
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if self.searching {
            return;
        }
        if (scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height
        {
            let difference = (scrollView.contentOffset.y + scrollView.frame.size.height)-scrollView.contentSize.height
            if difference > 0 && !(loadingView?.shouldAnimate)!
            {
                loadingView?.isHidden = false
                loadingView?.Reset()
                if difference <= 33
                {
                    loadingView?.circle1.alpha = 1
                }
                else if difference > 33 && difference <= 66
                {
                    loadingView?.circle1.alpha = 1
                    loadingView?.circle2.alpha = 1
                }
                else
                {
                    loadingView?.circle1.alpha = 1
                    loadingView?.circle2.alpha = 1
                    loadingView?.circle3.alpha = 1
                    loadingView?.startAnimate()
                    self.findMoviesWithQuery(fromSearch: false, queryString: self.searchTextField.text!)
                }
                
            }
            else
            {
                loadingView?.isHidden = true
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if listingHistory{
            listingHistory = false
            let queryString = arrayHistory[indexPath.row]
            self.searchTextField.text = queryString
            self.findMoviesWithQuery(fromSearch: true, queryString: queryString)
        }
    }
    
}

// MARK: - SearchTextField delegate -------------------
extension SearchMovesViewController: UITextFieldDelegate
{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let typeCasteToStringFirst = textField.text as NSString?
        let newString =  typeCasteToStringFirst?.replacingCharacters(in: range, with: string)
        print( String.init(format: "%@", newString!))
        if searching
        {
            Manager.stopAllRequests()
            searching = false
        }
        self.searchPage = 1;
        listingHistory = false
        self.findMoviesWithQuery(fromSearch: true, queryString: String.init(format: "%@", newString!))
        return true;
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if self.keyBoardHeight > 0.0 {
            self.resultTaleView.frame.size.height = self.view.frame.size.height - self.resultTaleView.frame.origin.y - self.keyBoardHeight+1;
            
        }
        return true;
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.resultTaleView.frame.size.height =   self.resultTaleView.frame.size.height + self.keyBoardHeight-1;
        if searchTextField.text != ""{
            Manager.saveSeachToHistory(queryString: searchTextField.text!.trimmingCharacters(in: CharacterSet.whitespaces))
        }
        return true;
    }
}
