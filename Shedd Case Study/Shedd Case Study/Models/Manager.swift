//
//  Manager.swift
//  Shedd Case Study
//
//  Created by Admin on 09/08/2018.
//  Copyright © 2018 Shedd. All rights reserved.
//

import UIKit
import Alamofire
class Manager: NSObject {
    
    static let shared = Manager()
    private override init() { }
    public static func findMoviesFromApiRest(queryString:String, page:Int, completion:@escaping (_ fvResponse: [NSDictionary], _ fvError: Error?) -> Void){
        
        let parameters: Parameters =  ["limit":10,"extended":"full"]
        let headers: HTTPHeaders = [
            "content-type": "application/json",
            "trakt-api-version": "2",
            "trakt-api-key": "019a13b1881ae971f91295efc7fdecfa48b32c2a69fe6dd03180ff59289452b8"
        ]
        let url = String.init(format: "https://api.trakt.tv/movies/popular?page=%i&query=%@", page,queryString).addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        Alamofire.request(url!,  parameters: parameters, headers: headers).responseJSON { response in
           
            if response.result.error != nil
            {
                completion([], response.result.error)
            }
           else if let json = response.result.value  {
                completion(json as! [NSDictionary], response.result.error)
            }
        }
    }
    
    public static func stopAllRequests(){
        
        if #available(iOS 9.0, *) {
            Alamofire.SessionManager.default.session.getAllTasks { (tasks) in
                tasks.forEach{ $0.cancel() }
            }
        } else {
            Alamofire.SessionManager.default.session.getTasksWithCompletionHandler { (sessionDataTask, uploadData, downloadData) in
                sessionDataTask.forEach { $0.cancel() }
                uploadData.forEach { $0.cancel() }
                downloadData.forEach { $0.cancel() }
            }
        }
    }
    
    public static func saveSeachToHistory(queryString:String){
        
        if var dictHistory = UserDefaults.standard.dictionary(forKey: "historyList"){
            dictHistory[queryString] = true
            UserDefaults.standard.setValue(dictHistory, forKeyPath:  "historyList")
        }
        else
        {
            var dictHistory = [String:Bool]()
            dictHistory[queryString] = true
            UserDefaults.standard.setValue(dictHistory, forKeyPath:  "historyList")
        }
        UserDefaults.standard.synchronize()
    }
    
    public static func getSeachToHistory()->[String]{
        
        if let dictHistory = UserDefaults.standard.dictionary(forKey: "historyList"){
            let arrayHistory = Array(dictHistory.keys)
            return arrayHistory
        }
        return []
    }
}
