//
//  CellFilm.swift
//  Shedd Case Study
//
//  Created by Admin on 09/08/2018.
//  Copyright © 2018 Shedd. All rights reserved.
//

import UIKit
import SDWebImage
class CellFilm: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    @IBOutlet weak var filmImageView: UIImageView!
    
     var item: NSDictionary? {
        didSet {
            guard let item = item else {
                return
            }
            if item["title"] != nil && !(item["title"] is NSNull)
            {
                self.titleLabel?.text = (item.value(forKey: "title") as! String)
            }
            if item["overview"] != nil && !(item["overview"] is NSNull)
            {
                self.overviewLabel.text = (item.value(forKey: "overview") as! String)
            }
            if item["year"] != nil && !(item["year"] is NSNull)
            {
                self.yearLabel.text =  NSString.init(format: "%@", item.value(forKey: "year") as! CVarArg) as String
            }
            
            let dictIds = item["ids"] as! NSDictionary
            if dictIds["imdb"] != nil && !(dictIds["imdb"] is NSNull){
                let imdbID = dictIds["imdb"] as! String
                let urlForPoster = "http://img.omdbapi.com/?apikey=985eef78&i=" + (imdbID)
                self.filmImageView.sd_setImage(with: URL.init(string: urlForPoster), placeholderImage: UIImage.init(named: "lauch_image"))
            }
            else
            {
                self.filmImageView.image = UIImage.init(named: "lauch_image")
            }
           
        }
    }
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        filmImageView?.image = nil
    }

}
